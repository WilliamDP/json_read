import RPi.GPIO as GPIO
import urllib
import json
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT)

while True:
    url = "http://192.168.20.207:5000"
    response = urllib.urlopen(url)
    data = json.loads(response.read())
    state = data['state']

    if(state == 0):
        GPIO.output(7, GPIO.LOW)
    else:
        GPIO.output(7, GPIO.HIGH)
    print data['state']
    time.sleep(2)
